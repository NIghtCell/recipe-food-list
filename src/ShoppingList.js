import React, { Component } from "react";
import styled from "styled-components";

const Button = styled.button`
  padding: 1rem;
  color: white;
  background-color: coral;
  font-weight: bold;
  font-size: 1rem;
  box-sizing: border-box;
  @media (max-width: 768px) {
    width: calc(100% - 2rem);
    margin-left: 1rem;
  }
`;

const Checkbox = styled.input`
  margin-left: 0.5rem;
`;

export default class ShoppingList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIngredients: []
    };
  }

  onCheckboxClick = e => {
    const ingredient = e.target.value;
    const isChecked = e.target.checked;
    if (isChecked) {
      this.addToSelectedIngredients(ingredient);
    } else {
      this.removeFromSelectedIngredients(ingredient);
    }
  };

  addToSelectedIngredients = ingredient => {
    let currentIngredients = this.state.selectedIngredients.slice();
    currentIngredients.push(ingredient);
    this.setState({
      selectedIngredients: currentIngredients
    });
  };

  removeAllSelectedIngredients = ingredients => {
    ingredients.forEach(ingredient =>
      this.removeFromSelectedIngredients(ingredient)
    );
  };

  removeFromSelectedIngredients = ingredient => {
    let currentIngredients = this.state.selectedIngredients.slice();
    currentIngredients = currentIngredients.filter(
      element => element != ingredient
    );
    this.setState({
      selectedIngredients: currentIngredients
    });
  };

  onRemoveSelectedClick = () => {
    this.props.removeSelected(this.state.selectedIngredients);
    this.setState({
      selectedIngredients: []
    });
  };

  onRemoveAllClick = () => {
    this.props.removeAllIngredients();
  };

  render() {
    const ingredientList = this.props.shoppingList.ingredients.map(
      (ingredient, key) => (
        <>
          <li key={ingredient}>
            {ingredient}
            <Checkbox
              onChange={this.onCheckboxClick}
              type="checkbox"
              id={key}
              name={ingredient}
              value={ingredient}
            />
          </li>
        </>
      )
    );
    return (
      <>
        <h3>Shopping list</h3>
        <ul>{ingredientList}</ul>
        <Button onClick={this.onRemoveSelectedClick}>Remove selected</Button>
        <Button onClick={this.onRemoveAllClick}>Clear shopping list</Button>
        <Button>Add ingredient</Button>
      </>
    );
  }
}
