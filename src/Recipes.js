import React, { Component } from "react";
import styled from "styled-components";

const Button = styled.button`
  padding: 0.5rem;
  color: black;
  background-color: white;
  font-weight: bold;
  font-size: 1rem;
`;

const Li = styled.li`
  font-size: 1.5rem;
  margin-bottom: 0.5rem;
`;

const RecipesContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  flex-flow: row wrap;
`;

const RecipeContainer = styled.div`
  padding: 1rem;
  background-color: coral;
  color: white;
  margin-bottom: 2rem;
  margin-right: 1rem;
  width: 48%;
  @media (max-width: 768px) {
    width: 100%;
    margin: 1rem 1rem;
  }
`;

export default class Recipes extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.show) {
      const recipes = this.props.recipes.map((recipe, key) => (
        <RecipeContainer key={key} className="recipe">
          <h5>Ingredients:</h5>
          <ul>
            {recipe.ingredients.map((ingredient, key) => (
              <Li key={key}>{ingredient}</Li>
            ))}
          </ul>
          <Button
            onClick={() => {
              this.props.onAddListBtnClick(recipe);
            }}
          >
            Add to shopping list
          </Button>
        </RecipeContainer>
      ));
      return (
        <div>
          <h3>All Recipes</h3>
          <RecipesContainer className="recipes">{recipes}</RecipesContainer>
        </div>
      );
    } else return null;
  }
}
