import React from "react";
import "./Toolbar.css";
import "./SideDrawer";
import DrawerToggleButton from "./DrawerToggleButton";
import "./Database";
import Database from "./Database";
import { Link } from "react-router-dom";

export default function(props) {
  return (
    <header className="toolbar">
      <nav className="toolbar__navigation">
        <div className="toolbar__toggle-button">
          <DrawerToggleButton click={props.drawerClickHandler} />
        </div>
        <div className="spacer"></div>
        <div className="toolbar__navigation-items">
          <ul>
            <Link to="/recipes">
              <li>Recipes</li>
            </Link>
            <Link to="/shopping-list">
              <li>Shopping List</li>
            </Link>
            <li>
              <a onClick={Database.logout} href="/">
                Logout
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
}
