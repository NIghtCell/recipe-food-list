import fire from "./config/fire";

const Database = {
  updateShoppingListDB: (user, ingredients) => {
    const db = fire.firestore();
    Database.getUserShoppingListId(user).then(userShoppingListId => {
      if (userShoppingListId != null) {
        db.collection("shopping-list")
          .doc(userShoppingListId)
          .update({
            user: user.email,
            ingredients: ingredients
          });
      }
    });
  },

  getUserShoppingListId: user => {
    return new Promise((resolve, reject) => {
      const db = fire.firestore();
      db.collection("shopping-list")
        .where("user", "==", user.email)
        .get()
        .then(doc => {
          if (doc && doc.docs.length < 2) {
            resolve(doc.docs[0].id);
          } else {
            reject("To many documents");
          }
        })
        .catch(err => {
          reject("Failure");
        });
    });
  },

  persistIngredientsOnShoppingList: (user, ingredients) => {
    const db = fire.firestore();
    Database.getUserShoppingListId(user).then(userShoppingListId => {
      if (userShoppingListId != null) {
        db.collection("shopping-list")
          .doc(userShoppingListId)
          .update({
            user: user.email,
            ingredients: ingredients
          });
      } else {
        db.collection("shopping-list")
          .doc()
          .set({
            user: user.email,
            ingredients: ingredients
          });
      }
    });
  },

  persistRecipe: recipe => {
    const db = fire.firestore();
    db.collection("recipes")
      .doc()
      .set(recipe);
  },

  loadShoppingList: user => {
    return new Promise((resolve, reject) => {
      const db = fire.firestore();
      db.collection("shopping-list")
        .where("user", "==", user.email)
        .get()
        .then(querySnapshot => {
          const shoppingList = querySnapshot.docs.map(doc => doc.data());
          const ingredients = shoppingList[0].ingredients;
          let shoppingListIngredients = [];
          if (ingredients.length > 0) {
            for (let ingredient in ingredients) {
              shoppingListIngredients.push(ingredients[ingredient]);
            }
          }
          resolve(shoppingListIngredients);
        })
        .catch(err => {
          console.error("Error reading shopping list: ", err);
          reject("Failure");
        });
    });
  },

  loadRecipes: user => {
    return new Promise((resolve, reject) => {
      const db = fire.firestore();
      db.collection("recipes")
        .where("user", "==", user.email)
        .get()
        .then(querySnapshot => {
          const recipes = querySnapshot.docs.map(doc => doc.data());
          let newState = [];
          for (let recipe in recipes) {
            newState.push({
              user: recipes[recipe].user,
              ingredients: recipes[recipe].ingredients
            });
          }
          resolve(newState);
        })
        .catch(err => {
          console.error("Error reading document: ", err);
          reject("Failure");
        });
    });
  },

  logout(e) {
    fire.auth().signOut();
  }
};

export default Database;
