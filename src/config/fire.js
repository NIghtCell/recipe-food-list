import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyC9O-kiPH5_2mgvpBR6uORI3_iHV1G-keg",
  authDomain: "recpie-shopping-list.firebaseapp.com",
  databaseURL: "https://recpie-shopping-list.firebaseio.com",
  projectId: "recpie-shopping-list",
  storageBucket: "recpie-shopping-list.appspot.com",
  messagingSenderId: "891288780973",
  appId: "1:891288780973:web:7e27556307e6a4adeec624",
  measurementId: "G-YSZ5NQ7VXL"
};

const fire = firebase.initializeApp(firebaseConfig);

export default fire;
