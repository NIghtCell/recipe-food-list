import React, { Component } from "react";
import RecipeAdder from "./RecipeAdder";
import Recipes from "./Recipes";
import styled from "styled-components";
import ShoppingList from "./ShoppingList";
import Database from "./Database";
import Toolbar from "./Toolbar";
import SideDrawer from "./SideDrawer";
import Backdrop from "./Backdrop";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

const Button = styled.button`
  padding: 1rem;
  color: white;
  background-color: coral;
  font-weight: bold;
  font-size: 1rem;
  box-sizing: border-box;
  @media (max-width: 768px) {
    width: calc(100% - 2rem);
    margin-left: 1rem;
  }
`;

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recipes: [],
      show: {
        recipeAdder: false,
        recipes: true
      },
      shoppingList: {
        user: this.props.user,
        ingredients: []
      },
      sideDrawerOpen: false
    };
  }

  componentDidMount() {
    Database.loadShoppingList(this.props.user).then(shoppingListIngredients => {
      this.setState({
        shoppingList: {
          ingredients: shoppingListIngredients
        }
      });
    });
    Database.loadRecipes(this.props.user).then(newState => {
      this.setState({
        recipes: newState
      });
    });
  }

  drawerToggleClickHandler = () => {
    this.setState(prevState => {
      return { sideDrawerOpen: !prevState.sideDrawerOpen };
    });
  };

  backDropClickHandler = () => {
    this.setState({
      sideDrawerOpen: false
    });
  };

  addRecipe = ingredients => {
    const tempRecipes = this.state.recipes.slice();
    let newRecipe = {
      user: this.props.user.email,
      ingredients: ingredients
    };
    tempRecipes.unshift(newRecipe);
    this.setState({
      recipes: tempRecipes
    });
    Database.persistRecipe(newRecipe);
  };

  onRecipeAdderClick = () => {
    this.setState({
      show: {
        recipes: false,
        recipeAdder: true
      }
    });
  };

  showRecipes = () => {
    this.setState({
      show: {
        recipes: true,
        recipeAdder: false
      }
    });
  };

  onAddListBtnClick = recipe => {
    this.addToShoppingList(recipe);
  };

  addToShoppingList = recipe => {
    const ingredients = recipe.ingredients;
    const currentIngredients = this.state.shoppingList.ingredients.slice();
    ingredients.forEach(ingredient => {
      this.addIngredientToShoppingList(currentIngredients, ingredient);
    });
    this.setState({
      shoppingList: {
        ingredients: currentIngredients
      }
    });
    Database.persistIngredientsOnShoppingList(
      this.props.user,
      currentIngredients
    );
  };

  addIngredientToShoppingList = (ingredients, ingredient) => {
    if (!this.hasIngredient(ingredients, ingredient)) {
      ingredients.push(ingredient);
    }
  };

  hasIngredient = (ingredients, ing) => {
    const result = ingredients.filter(ingredient => ingredient === ing);
    return result.length > 0;
  };

  removeSelected = ingredients => {
    let currentIngredients = this.state.shoppingList.ingredients.slice();
    ingredients.forEach(ingredient => {
      currentIngredients = currentIngredients.filter(
        element => element !== ingredient
      );
    });
    this.setState({
      shoppingList: {
        ingredients: currentIngredients
      }
    });
    Database.updateShoppingListDB(this.props.user, currentIngredients);
  };

  removeAllIngredients = () => {
    this.setState({
      shoppingList: {
        ingredients: []
      }
    });
    Database.updateShoppingListDB(this.props.user, []);
  };

  render() {
    return (
      <Router>
        <div style={{ height: "100%" }}>
          <Toolbar drawerClickHandler={this.drawerToggleClickHandler} />
          {this.state.sideDrawerOpen && (
            <>
              <Backdrop click={this.backDropClickHandler} />
            </>
          )}
          <SideDrawer show={this.state.sideDrawerOpen} />
          <main style={{ "margin-top": "70px", padding: "0 1rem" }}>
            <Switch>
              <Route
                path="/shopping-list"
                render={props => (
                  <ShoppingList
                    {...props}
                    removeAllIngredients={this.removeAllIngredients}
                    removeSelected={this.removeSelected}
                    shoppingList={this.state.shoppingList}
                  />
                )}
              />
              <Route
                path="/recipes"
                render={props => (
                  <Recipes
                    {...props}
                    onAddListBtnClick={this.onAddListBtnClick}
                    user={this.props.user}
                    show={this.state.show.recipes}
                    recipes={this.state.recipes}
                  />
                )}
              />
              <Redirect exact from="/" to="shopping-list" />
            </Switch>
            <RecipeAdder
              showRecipes={this.showRecipes}
              show={this.state.show.recipeAdder}
              addRecipe={this.addRecipe}
            />
            {!this.state.show.recipeAdder && (
              <Button onClick={this.onRecipeAdderClick}>Add recipe</Button>
            )}
          </main>
        </div>
      </Router>
    );
  }
}
