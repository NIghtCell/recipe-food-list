import React from "react";

import "./Backdrop.css";

export default function(props) {
  return <div className="backdrop" onClick={props.click}></div>;
}
