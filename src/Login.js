import React, { Component } from "react";
import fire from "./config/fire";
import styled from "styled-components";

const ChildContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 65%;
  @media (max-width: 767px) {
    width: 90%;
  }
`;

const Input = styled.input`
  text-align: center;
  width: 100%;
  padding: 1rem;
  margin: 0.5rem 0;
  border: 1px solid grey;
`;

const Button = styled.button`
  background-color: coral;
  color: white;
  border: none;
  padding: 1rem;
  font-weight: bold;
  margin: 0.5rem 0;
  width: 100%;
`;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      email: "",
      password: ""
    };
  }

  login(e) {
    e.preventDefault();
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(user => {
        console.log(user);
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  signup(e) {
    e.preventDefault();
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(user => {
        console.log(user);
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <ChildContainer>
        <h3 className="mb-3">
          Please sign in with your account, or create a new one!
        </h3>
        <form>
          <div className="row">
            <div className="col-md-6">
              <Input
                type="email"
                id="email"
                name="email"
                placeholder="email address"
                onChange={this.handleChange}
                value={this.state.email}
              />
            </div>
            <div className="col-md-6">
              <Input
                name="password"
                type="password"
                id="password"
                onChange={this.handleChange}
                placeholder="password"
                value={this.state.password}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <Button onClick={this.login}>Login</Button>
            </div>
            <div className="col-md-6">
              <Button onClick={this.signup}>Signup</Button>
            </div>
          </div>
        </form>
      </ChildContainer>
    );
  }
}
