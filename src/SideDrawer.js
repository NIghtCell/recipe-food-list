import React from "react";

import "./SideDrawer.css";
import Database from "./Database";
import { Link } from "react-router-dom";

export default function(props) {
  let drawerClasses = ["side-drawer"];
  if (props.show) {
    drawerClasses = ["side-drawer open"];
  }
  return (
    <nav className={drawerClasses}>
      <ul>
        <Link to="/recipes">
          <li>Recipes</li>
        </Link>
        <Link to="/shopping-list">
          <li>Shopping list</li>
        </Link>
        <li>
          <a onClick={Database.logout} href="/">
            Logout
          </a>
        </li>
      </ul>
    </nav>
  );
}
