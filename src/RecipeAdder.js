import React, { Component } from "react";
import styled from "styled-components";

const Input = styled.input`
  text-align: center;
  width: 100%;
  padding: 1rem;
  margin: 0.5rem 0;
  border: 1px solid grey;
`;

const Button = styled.button`
  background-color: coral;
  color: white;
  border: none;
  padding: 1rem;
  font-weight: bold;
  margin: 0.5rem 0;
  width: 100%;
`;

const IngredientAdder = styled.div`
  margin: 20px 20px;
  @media (min-width: 768px) {
    width: 65%;
    margin: 20px auto;
  }
`;

const Li = styled.li`
  font-size: 1.5rem;
`;

export default class RecipeAdder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingredients: [],
      ingredient: ""
    };
  }

  onIngredientChange = e => {
    this.setState({
      ingredient: e.target.value
    });
  };

  onAddIngredientClick = e => {
    this.addIngredient();
    this.resetIngredientInput();
  };

  onAddRecipeClick = e => {
    this.props.addRecipe(this.state.ingredients);
    this.props.showRecipes();
  };

  addIngredient = () => {
    const tempIngredients = this.state.ingredients.slice();
    tempIngredients.push(this.state.ingredient);
    this.setState({
      ingredients: tempIngredients
    });
  };

  resetIngredientInput = () => {
    this.setState({
      ingredient: ""
    });
  };

  render() {
    if (this.props.show) {
      const ingriedients = this.state.ingredients.map(ingredient => (
        <Li key={ingredient}>{ingredient}</Li>
      ));

      return (
        <IngredientAdder>
          <h3>Ingredients: </h3>
          <div className="ingredients">
            <ul>{ingriedients}</ul>
          </div>
          <div className="ingredient-add">
            <div className="row">
              <div className="col-md-6">
                <Input
                  type="text"
                  name="ingredient"
                  id="ingredient"
                  placeholder="Ingredient"
                  onChange={this.onIngredientChange}
                  value={this.state.ingredient}
                />
              </div>
              <div className="col-md-6">
                <Button onClick={this.onAddIngredientClick}>
                  Add Ingredient
                </Button>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <Button onClick={this.onAddRecipeClick}>Add Recipe</Button>
              </div>
            </div>
          </div>
        </IngredientAdder>
      );
    } else return null;
  }
}
